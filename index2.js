//--- Soal 2 ---

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
// Lanjutkan code untuk menjalankan function readBooksPromise

//--- Jawaban soal 2 ---
readBooksPromise(10000, books[0])
    .then(function (sisa) {
        readBooksPromise(sisa, books[1])
            .then(function (sisa) {
                readBooksPromise(sisa, books[2])
                    .then(function (sisa) {
                        readBooksPromise(sisa, books[3])
                            .then(function (sisa) {
                            console.log ("---selesai tepat waktu--")
                            })
                            .catch(error => console.log(error))
                })
                .catch(error => console.log(error))
            })
        .catch(error => console.log(error))
    })
.catch(error => console.log(error))
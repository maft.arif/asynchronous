//--- Soal 1 ---

var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
// Tulis code untuk memanggil function readBooks di sini

//--- Jawaban soal 1 ---
readBooks(10000, books[0], function (waktusisa) {
    readBooks(waktusisa, books[1], function (waktusisa) {
        readBooks(waktusisa, books[2], function (waktusisa) {
            readBooks(waktusisa, books[3], function (waktusisa) {
                console.log ("---selesai tepat waktu--")
            })
        })
    })
})

